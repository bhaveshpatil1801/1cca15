package OverRiding;

import java.util.Scanner;

public class MainFlipAma {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter Qty");
        int qty=sc1.nextInt();
        System.out.println("Enter price");
        double price=sc1.nextDouble();
        System.out.println("Select Platform");
        System.out.println("1:Flipcart\n2:Amazon");
        int choice=sc1.nextInt();
        if (choice == 1) {
            Flipcart f1=new Flipcart();
            f1.sellProduct(qty,price);
        }
        else if(choice==2){
            Amazon a1=new Amazon();
            a1.sellProduct(qty,price);
        }
        else{
            System.out.println("Invalid Choice");
        }
    }
}
