package ExceptionHandling;


import java.net.MalformedURLException;
import java.net.URL;

import static com.sun.webkit.network.URLs.newURL;


public class ExceptionDemo8 {


    private static Object URL ="" ;

    public static void main(String[] args) {
        //checked Exception
        String link="https://mail.google.com/mail/u/0/";
        try{
           URL = newURL(link);

        } catch (MalformedURLException e) {

            System.out.println(e);

        }
    }
}
