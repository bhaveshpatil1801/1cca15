package ExceptionHandling;


import java.io.FileWriter;
import java.io.IOException;

public class ExceptionDemo7 {
    public static void main(String[] args) {
        //checked Exception
        String str = "Java is object oriented programming language";

        FileWriter fw = null;
        try {
            fw = new FileWriter("D:\\Exception\\java\\.txt");
            fw.write(str);
            fw.close();
            System.out.println("File created successfully");

        }catch(IOException e){
            System.out.println(e);
        }
    }

}
