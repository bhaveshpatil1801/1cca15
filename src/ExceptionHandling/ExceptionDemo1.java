package ExceptionHandling;


public class ExceptionDemo1 {
    public static void main(String[] args) {
        System.out.println("Program Started");
        String s1 = "ABC";

        try {
            int no1 = Integer.parseInt(s1);//abnormal
            System.out.println("No1:" + no1);

        } catch (NumberFormatException n) {
            System.out.println(n);
        }
        System.out.println("Program Ended");
    }
}