package ExceptionHandling;

public class ExceptionDemo2 {
    public static void main(String[] args) {
        System.out.println("Program Started");
        String s1=null;
        try{
            System.out.println(s1.toLowerCase());
        }
        catch(NullPointerException a){
            System.out.println(a);
        }


        System.out.println("Program Ended");
    }
}
