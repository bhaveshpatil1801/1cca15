package ExceptionHandling;

import java.util.Scanner;

public class ExceptionDemo9 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter Qty");
        int qty = sc1.nextInt();
        System.out.println("Enter Price");
        double price = sc1.nextDouble();
        billCalculation(qty,price);
    }
        static void billCalculation(int qty,double price){
            if(qty>0&&price>0){
                double total=price*qty;
                System.out.println(total);
        }
           else{

               throw new  IllegalArgumentException("Invalid Position");
            }
    }
}
