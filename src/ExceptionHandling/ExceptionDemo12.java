package ExceptionHandling;

import java.util.Scanner;

public class ExceptionDemo12 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter Email Id");
        String Email=sc1.next();
        validation(Email);


    }
    static void validation(String email) throws InvaidEmailException{
        if(email.contains("@")&&email.contains(".")){
            System.out.println("Validation Successful");
        }
        else {
            throw new InvaidEmailException("Invalid Email Id");
        }
    }
}
