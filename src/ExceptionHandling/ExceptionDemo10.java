package ExceptionHandling;

public class ExceptionDemo10 {
    static void test(){
        System.out.println("Test Started");
        int c=10/0;//exception origin
        System.out.println("Test Ended");
    }
    static void display(){

        System.out.println("Display Started");
        try{
            test();
        }
        catch(ArithmeticException a){
            System.out.println(a);
        }

        System.out.println("Display Ended");
    }
    static void info(){
        System.out.println("Info Started");
        display();
        System.out.println("Info Ended");
    }

    public static void main(String[] args) {
        System.out.println("Main Started");
        info();
        System.out.println("Main Ended");
    }

}
