package ExceptionHandling;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionDemo3 {
    public static void main(String[] args) {
        System.out.println("Program Started");
        try {
            Scanner sc1 = new Scanner(System.in);
            System.out.println("Enter No 1");
            int n1 = sc1.nextInt();
            System.out.println("Enter No 2");
            int n2 = sc1.nextInt();
            int result = n1 * n2;
            System.out.println(result);
        }
        catch(InputMismatchException a){
            System.out.println(a);
        }

        System.out.println("Program Ended");
    }
}
