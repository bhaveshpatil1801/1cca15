package ExceptionHandling;

import java.io.IOException;

public class ExceptionDemo11 {
    static void test()throws IOException{
        throw new IOException();

    }

    public static void main(String[] args) {
        System.out.println("Main Started");
        try{
            test();
        } catch (IOException e) {
            System.out.println(e);;
        }
        System.out.println("MAin Ended");
    }

}
