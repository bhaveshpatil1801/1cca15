package ExceptionHandling;

public class ExceptionDemo5 {
    public static void main(String[] args) {
        System.out.println("Program Started");
        int []data=new int[3];
        String s1=null;

      try{   //outer
          try{
              data[1]=20;
              data[4]=40;
              System.out.println(data[1]+"\t"+data[4]);
          }
          catch(ArrayIndexOutOfBoundsException a){
              System.out.println(a);
          }
          System.out.println(s1.toLowerCase());

      }
      catch(NullPointerException b){
          System.out.println(b);

      }
        //System.out.println(s1);
        System.out.println("Program Ended");

    }
}
