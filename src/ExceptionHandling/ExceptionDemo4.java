package ExceptionHandling;

public class ExceptionDemo4 {
    public static void main(String[] args) {
        System.out.println("Program Started");
        String s1=null;
        try{
            System.out.println("Try Started");
            System.out.println(s1.length());
            System.out.println("Try Ended");
        }
        catch(NullPointerException a){
            System.out.println(a);
        }

    }
}
