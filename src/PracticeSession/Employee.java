package PracticeSession;

public class Employee {
    private int empId=101;
    private double empSalary=10000;
    //read Access
    public int getEmpId(){
        return empId;

    }
    //Write Access
    void setEmpId(int empID){
        this.empId=empId;

    }
    //read Access
    public double getEmpSalary() {
  return empSalary;
    }
    //write Access
    void setEmpSalary(double empSalary){
        if(empSalary>0){
            this.empSalary=empSalary;
        }
        else{
            System.out.println("Invalid Salary");
        }
    }
    }

