package PracticeSession;

public class PhonePay implements UPI, Wallet{
    @Override
    public void transferAmount(double amt) {
        System.out.println("TransferAmount:"+amt);
    }

    @Override
    public void billPayments(double amt) {
        System.out.println("BillPayments:"+amt);
    }
}
