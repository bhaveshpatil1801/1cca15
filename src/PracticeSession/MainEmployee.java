package PracticeSession;

public class MainEmployee {
    public static void main(String[] args) {
        Employee e1=new Employee();
        int id= e1.getEmpId();//read Private Data
        double Salary=e1.getEmpSalary();
        System.out.println("ID:"+id);
        System.out.println("Salary:"+Salary);

        //modify private data through setter
        e1.setEmpId(201);
        e1.setEmpSalary(20000);
        System.out.println(e1.getEmpSalary());
        System.out.println(e1.getEmpId());
    }
}
