package Parsing;

public class ParsingDemo {
    public static void main(String[] args) {
        String s1="20";
        String s2="30";
        System.out.println(s1+s2);

        //Parsing String into Primitive Format
        int No1=Integer.parseInt(s1);
        int No2=Integer.parseInt(s2);
        System.out.println(No1+No2);
        //Parsing Primitive to String format
        double d1=20.0;
        double d2=30.1;
        System.out.println(d1+d2);
                String str=Double.toString(d1);
                String str1=Double.toString(d2);
        System.out.println(str+str1);


    }
}
