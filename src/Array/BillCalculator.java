package Array;



public class BillCalculator {
    void calculateBill(double[]amounts)
    {
        double[]gstValues=gstCalculation(amounts);
        double[]totalAmounts=new double [amounts.length];
        for(int a=0;a<amounts.length;a++){
            totalAmounts[a]=amounts[a]+gstValues[a];
        }
        double totalBillAmount=0.0;
        double totalgstBill=0.0;
        double totalfinalAmount=0.0;

        for(int a=0;a<amounts.length;a++){
            totalBillAmount+=amounts[a];
            totalgstBill+=amounts[a];
            totalfinalAmount+=amounts[a];
        }
        System.out.println("BillAmt\tGSTAmt\tTotal");
        System.out.println("===============================");
        for(int a=0;a<amounts.length;a++){
            System.out.println(amounts[a]+"\t"+gstValues+"\t"+totalBillAmount);
    }
        System.out.println("=====================================");
        System.out.println(totalBillAmount+"\t"+totalgstBill+"\t"+totalfinalAmount);

    }
    double[]gstCalculation(double[]amounts ){
        double[]gstAmounts=new double [amounts.length];
        for(int a=0;a<amounts.length;a++){
            if(amounts[a]<500){
                gstAmounts[a]=amounts[a]*0.05;
            }
            else {
                gstAmounts[a] = amounts[a] * 0.1;
            }
        }
        return gstAmounts;

    }
}
