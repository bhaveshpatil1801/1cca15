package Array;

import java.util.Scanner;

public class ArrayDemo12 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        BillCalculator b1=new BillCalculator();
        System.out.println("Enter no of bills");
        int count =sc1.nextInt();
        System.out.println("Enter Bill Amounts");
        double []amounts=new double[count];
        for(int a=0;a<amounts.length;a++){
            amounts[a]=sc1.nextInt();
        }
       b1.calculateBill(amounts);
    }
}
