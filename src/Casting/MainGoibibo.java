package Casting;

import java.util.Scanner;

public class MainGoibibo {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter Service provide");
        System.out.println("1:AirAsia\n2:AirIndia");
        int choice=sc1.nextInt();
        System.out.println("Select Route");
        System.out.println("0:Jalgaon-Nashik");
        System.out.println("1:Pune-Mumbai");
        int routechoice=sc1.nextInt();
        System.out.println("Enter No of Tickets");
        int tickets=sc1.nextInt();

        Goibibo g1=null;
        if(choice==1){
            g1=new AirAsia();
        }
        else if(choice ==2){
            g1=new AirIndia();
        }
        g1.bookTicket(tickets,routechoice);
    }
}
