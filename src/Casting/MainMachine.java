package Casting;

import java.util.Scanner;

public class MainMachine {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter Qty");
        int qty= sc1.nextInt();
        System.out.println("Enter Price");
        double price=sc1.nextDouble();
        System.out.println("Select Machine");
        System.out.println("1:Laptop\n2:Projector");
        int choice=sc1.nextInt();
        Machine m1=null;
        if(choice==1){
            m1=new Laptop();//upcasting
        }
        else if(choice==2){
            m1=new Projector();//upcasting
        }
        m1.getType();
        m1.calculateBill(qty,price);
    }
}
