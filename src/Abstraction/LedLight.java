package Abstraction;

public class LedLight implements Switch {

    @Override
    public void switchOn() {
        System.out.println("LED Light is On");
    }

    @Override
    public void switchOff() {
        System.out.println("CFL Light is Off");
    }
}
