package Abstraction;

public interface Switch {
    public void switchOn();
    public void switchOff();

}
