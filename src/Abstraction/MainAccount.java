package Abstraction;

import java.util.Scanner;

public class MainAccount {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Select Account Type");
        System.out.println("1:Saving\n2:Loan");
        int accType=sc1.nextInt();
        System.out.println("Enter Acc Opening Balance");
        double balance=sc1.nextDouble();
        AccountFactory Factory=new AccountFactory();
        Account accRef=Factory.createAccount(accType,balance);
        boolean status=true;
        while(status){
            System.out.println("Select Mode Of Transaction");
            System.out.println("1:Deposit\n2:WithDraw\n3:checkBalance\n4:Exit");
            int choice=sc1.nextInt();

            if(choice==1){
                System.out.println("Enter Amount");
                double amt=sc1.nextDouble();
                accRef.deposit(amt);
            }
            else if(choice==2){
                System.out.println("Enter Amount");
                double amt=sc1.nextDouble();
                accRef.withdraw(amt);
            } else if (choice==3) {
                double amt= sc1.nextDouble();
                accRef.checkBalance();

            }
            else{
                status=false;
            }
        }
    }
}
