package Abstraction;

public class LoanAccount implements Account{
    double loanAmount;
    //account creation
    public LoanAccount(double loanAccount){
        this.loanAmount=loanAmount;
        System.out.println("Loan Account Created");
    }
    @Override
    public void deposit(double amt) {
         loanAmount+=amt;
        System.out.println(amt+"Rs credited your Account");
    }

    @Override
    public void withdraw(double amt) {
        loanAmount-=amt;
        System.out.println(amt+"Rs Debited from Loan Account");
    }

    @Override
    public void checkBalance() {
        System.out.println("Active Loan Amount"+loanAmount);
    }
}
