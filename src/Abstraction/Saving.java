package Abstraction;

public class Saving implements Account{
  //step 2
  double accountBalance;

  public Saving(double accountBalance) {
     this.accountBalance=accountBalance;
    System.out.println("Saving account balence");
  }


  @Override
    public void deposit(double amt) {
    accountBalance+=amt;
    System.out.println(amt+"Rs Credited");
    }

    @Override
    public void withdraw(double amt) {
     if(amt<=accountBalance){
       accountBalance-=amt;
       System.out.println(amt+"Rs Debited");
     }
     else{
       System.out.println("Insufficient Balance");
     }
    }

  @Override
  public void checkBalance() {
    System.out.println("Active Balance"+accountBalance);
  }
}
