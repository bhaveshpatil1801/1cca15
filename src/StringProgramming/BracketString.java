package StringProgramming;

public class BracketString {
    public static void main(String[]args) {
        String str = "(){}[]";
        char[] arr = str.toCharArray();
        int n1 = arr.length;

        if (n1 % 2 == 0) {
            for (int i = 1; i < str.length(); i++) {
                int sum = arr[i] - arr[i - 1];
                if (sum < 3 && sum > 0) {
                    System.out.println("Bracket are Valid");
                }
            }
        }
    }

}
