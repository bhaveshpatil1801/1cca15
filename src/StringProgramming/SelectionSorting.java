package StringProgramming;

public class SelectionSorting {
    public static void main(String[] args) {
        int arr[]=RandomArray.array(5);
        for(int a:arr){
            System.out.println(a+"\t");

        }
        System.out.println();
        for(int i=0;i<arr.length;i++){
            int minInd=i;
            for(int j=i+1;j<arr.length;j++){
                if(arr[j]<arr[minInd]){
                    minInd=j;
                }
            }
            int temp=arr[minInd];
            arr[minInd]=arr[i];
            arr[i]=temp;
            System.out.println(arr[i]+"\t");
        }
    }
}
