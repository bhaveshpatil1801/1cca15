package StringProgramming;

import java.util.Arrays;

public class StringEnegram {
    public static void main(String[] args) {
        String str="core java";
        String str1="java core";
        int n1=str.length();
        int n2=str1.length();

        if(n1==n2){
            char[]arr=str.toCharArray();
            char[]arr1=str1.toCharArray();

            Arrays.sort(arr);
            Arrays.sort(arr1);

            boolean status=false;
            for(int i=0;i<n1;i++){
                if(arr[i]!=arr1[i]){
                    status=true;
                }
                if(status){
                    System.out.println("String is Anagram");
                }
                else{
                    System.out.println("String is Not Anagram");
                }
            }

        }
    }
}
