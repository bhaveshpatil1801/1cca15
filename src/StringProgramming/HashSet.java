package StringProgramming;

import java.util.Set;

public class HashSet {
    public static void main(String[] args) {
        Set<Integer>hs1=new java.util.HashSet<>();
        hs1.add(10);
        hs1.add(20);
        for(Integer i:hs1){
            System.out.println(i);


        }
        System.out.println("===================");
        Set<Character >hs2=new java.util.HashSet<>();
        String str="Core Java Programming";
        for(int i=0;i<str.length();i++){
            hs2.add(str.charAt(i));
        }
        for(Character c:hs2){
            System.out.println(c);
        }
    }
}
