package ObjectClass;

public class Book {
    int bookId;
    String bookName;
    double bookPrice;



    public  Book(int bookId,String bookName,double bookPrice){
        this.bookId=bookId;
        this.bookName=bookName;
        this.bookPrice=bookPrice;

    }



    //converting object into String
    public String toString(){
       return bookId+"\t"+bookName+"\t"+bookPrice;
    }

}
