package Abstract;

import java.util.Scanner;

public class MainEmployee {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Select Type");
        System.out.println("1:Employee\n2:WatchMan");
        int choice=sc1.nextInt();
        Employee e=null;
        if(choice==1){
            e=new Manager();

        }
        else if(choice==2){
            e=new WatchMan();
        }
        e.getDesignation();
        e.getSalary();




    }
}
