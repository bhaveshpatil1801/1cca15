package Abstract;

public class WatchMan extends Employee {

    @Override
    void getDesignation() {
        System.out.println("Desigation is WatchMan");
    }

    @Override
    void getSalary() {
        System.out.println("Salary is 30000");

    }
}
