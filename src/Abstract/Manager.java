package Abstract;

public class Manager extends Employee {
    @Override
    void getDesignation() {
        System.out.println("Designation is Manager");
    }

    @Override
    void getSalary() {
        System.out.println("Salari is 50000");

    }
}
