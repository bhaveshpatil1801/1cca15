package Abstract;

public abstract class Demo {
    //static and non-static variables
    static int a=10;
    double b=25.25;
    //non-static abstarct and concrete methods
        abstract void test();

        void display(){
            System.out.println("Display Method");
        }
        //static concrete method
        static void info(){
            System.out.println("Info Method");
        }
        //constructor
    Demo(){
        System.out.println("Constructor");
    }
    static{
        System.out.println("Static block");
    }
    {
        System.out.println("Non-Static block");
    }


}
