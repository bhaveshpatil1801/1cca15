package String;

import java.util.Scanner;

public class StringDemo5 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter String");

        String str=sc1.next();
        String newValue=str.toLowerCase();
        int vCount=0;
        int cCount=0;

        char[]str1=str.toCharArray();
        for(int a=0;a<str1.length;a++){
            if (str1[a] == 'a' || str1[a] == 'e' || str1[a] == 'i' || str1[a] == 'o' || str1[a]=='u') {
                vCount++;
            }
            else{
                cCount++;
            }
        }
        System.out.println("Total Vovels"+vCount);
        System.out.println("Total consonent"+cCount);
    }
}
