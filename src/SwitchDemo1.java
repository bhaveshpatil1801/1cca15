import java.net.Socket;
import java.util.Scanner;

public class SwitchDemo1 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Select Language");
        System.out.println("1:Java\n2:SQL\n3:Mongo");
        int choice = sc1.nextInt();
        {


            switch (choice) {
                case 1:
                    System.out.println("selected java");
                case 2:
                    System.out.println("Selected SQL");
                case 3:
                    System.out.println("Selected Mongo");
                default:
                    System.out.println("Invalid");
            }
        }
    }
}
