package CollectionFramework;

import java.util.Scanner;

public class ArrayListDemoBook7 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        BookOperation b=new BookOperation();
        boolean status=true;
        while(status){
            System.out.println("Select mode of operation");
            System.out.println("1:Add new Book");
            System.out.println("2:Display new Book");
            System.out.println("3:Delete existing book");
            System.out.println("4:Exit");

            int choice=sc1.nextInt();

            switch(choice){
                case 1:
                System.out.println("Enter book Id ");
                int id= sc1.nextInt();
                    System.out.println("Enter Book Name");
                    String name= sc1.next();
                    System.out.println("Enter book price");
                    double price= sc1.nextDouble();
                    b.addBook(id,name,price);
                    break;

                case 2:
                    b.displayBook();
                    break;

                case 3:
                    System.out.println("Enter Book Id");
                    int bookId= sc1.nextInt();
                    b.deleteBook(bookId);
                    break;

                case 4:
                    status=false;
                    break;


            }
        }
    }
}
