package CollectionFramework;

 import java.util.ArrayList;
 import java.util.Iterator;

public class ArrayListDemo6 {
    public static void main(String[] args) {
        ArrayList<Integer>data=new ArrayList<>();
        data.add(10);
        data.add(20);
        data.add(30);
        //fourth option for execution the code
        Iterator<Integer> itr=data.iterator();
        while(itr.hasNext()){
           if(itr.next()==20){
               itr.remove();
           }
        }
        System.out.println(data);
    }
}
