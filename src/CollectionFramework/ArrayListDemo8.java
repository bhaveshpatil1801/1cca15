package CollectionFramework;

import java.util.ArrayList;

public class ArrayListDemo8 {
    public static void main(String[] args) {
        ArrayList<Double>data1=new ArrayList<>();
        data1.add(10.1);
        data1.add(20.1);

        ArrayList<Double>data2=new ArrayList<>();
        data2.add(30.2);
        data2.add(40.2);

       data2.addAll(data1);
        System.out.println(data1);
    }
}
