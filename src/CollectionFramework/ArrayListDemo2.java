package CollectionFramework;

import java.util.ArrayList;

public class ArrayListDemo2 {
    public static void main(String[] args) {
        ArrayList <Integer> data=new ArrayList<Integer>();
        data.add(10);
        data.add(20);
        System.out.println(data);
        //option second
        for(int a:data){
            System.out.println(a);
        }
        //third option
        for(int a=0;a< data.size();a++){
            System.out.println(data.get(a));
        }
    }
}
