package CollectionFramework;

public class Book {
    int bookId;
    String bookName;
    double bookPrice;

    public Book(int bookId, String bookName, double bookPrice) {
        this.bookId = bookId;
        this.bookName = bookName;
        this.bookPrice = bookPrice;
    }

    @Override
    public String toString() {
       return bookId+"\t"+bookName+"\t"+bookPrice;
    }
}
