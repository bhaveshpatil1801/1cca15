package CollectionFramework;

import java.util.ArrayList;
import java.util.Collections;

public class ArrayListDemo9 {
    public static void main(String[] args) {
        ArrayList<Double>values=new ArrayList<>();

        values.add(12.9);
        values.add(30.5);
        values.add(36.73);
        values.add(10.1);

        System.out.println(values);
        //sorting in ascending order
        Collections.sort(values);
        System.out.println(values);


    }
}
