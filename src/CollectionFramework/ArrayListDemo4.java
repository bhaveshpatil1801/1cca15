package CollectionFramework;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayListDemo4 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter Start Point");
        int start=sc1.nextInt();
        System.out.println("Enter End Point");
        int end=sc1.nextInt();
        ArrayList<Integer>even= new ArrayList<>();
        ArrayList<Integer>odd=new ArrayList<>();
        for(int a=start;a<=end;a++){
            if(a%2==0){
                even.add(a);
            }
            else{
                odd.add(a);
            }

        }
        System.out.println(even);
        System.out.println(odd);

    }

}
