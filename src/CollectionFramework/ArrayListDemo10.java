package CollectionFramework;

import java.util.ArrayList;

public class ArrayListDemo10 {
    public static void main(String[] args) {
        product p1=new product(1,"TV",20000.2);
        product p2=new product(2,"AC",30000.2);
        product p3=new product(3,"Mobile",15000);

        //List of custom object
        ArrayList<product>data=new ArrayList<>();
        data.add(p1);
        data.add(p2);
        data.add(p3);
        for(product p:data){
            System.out.println(p);
        }
    }
}
