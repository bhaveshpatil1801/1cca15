package CollectionFramework;

import java.util.ArrayList;
import java.util.Iterator;

public class ArrayListDemo5 {
    public static void main(String[] args) {
        ArrayList<Character>data=new ArrayList<>();
        data.add('B');
        data.add('h');
        data.add('a');
        data.add('v');
        data.add('e');
        data.add('s');
        data.add('h');
        Iterator<Character> itr=data.iterator();
        while(itr.hasNext()){
            System.out.println(itr.next());
        }


    }
}
