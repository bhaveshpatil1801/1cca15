package CollectionFramework;

import java.util.ArrayList;
import java.util.Iterator;

public class BookOperation {
    //business logic class
    ArrayList<Book>bookList=new ArrayList<>();


    public void addBook(int id, String name, double price) {
        Book b1=new Book(id,name,price);
        bookList.add(b1);
        System.out.println("Book Added Successfully");

    }

    public void displayBook() {
        System.out.println("ID\tName\tPrice");
        System.out.println("===============");
        for(Book b:bookList){
            System.out.println(b);
        }
    }

    public void deleteBook(int bookId) {
        Iterator<Book>itr=bookList.iterator();
        while(itr.hasNext()){
            if(itr.next().bookId==bookId){
                itr.remove();
                System.out.println("Book Remove Successfully");
            }
        }
    }
}
