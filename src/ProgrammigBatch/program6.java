package ProgrammigBatch;

public class program6 {
    public static void main(String[] args) {
        int line=5;
        int star=1;
        int space=2;

        for(int i=0;i<line;i++){

            for(int j=0;j<space;j++){
                System.out.print("  ");

            }
            for(int k=0;k<star;k++){
               if(k==0||k==star-1){
                   System.out.print(" * ");
               }else{
                   System.out.print("  ");
                }

            }
            System.out.println();
            if(i<=1){
                star+=2;
                space--;
            }
            else{
                space++;
                star-=2;
            }
        }
    }

}
