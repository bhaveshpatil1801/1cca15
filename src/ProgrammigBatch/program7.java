package ProgrammigBatch;

public class program7 {
    public static void main(String[] args) {
        int line = 5;
        int star = 1;
        int space = 4;
        int ch=1;

        for (int i = 0; i < line; i++) {

            for (int j = 0; j < space; j++) {
                System.out.print("\t");

            }
            for (int k = 0; k < star; k++) {
                if (i==k) {
                    System.out.print(ch+"\t");
                } else {
                    System.out.print("*\t");
                }

            }
            System.out.println();
            ch++;
            star+=2;
            space--;

        }
    }
}
