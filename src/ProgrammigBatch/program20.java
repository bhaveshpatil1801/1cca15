package ProgrammigBatch;

public class program20 {
    public static void main(String[] args) {
        int line=5;
        int star=5;
        char ch='A';

        for(int a=0;a<line;a++){
            for(int b=0;b<star;b++){
                System.out.print(" "+ch++ +" ");
                if(ch>'E'){
                    ch='A';
                }
            }
            System.out.println();
        }

    }
}
