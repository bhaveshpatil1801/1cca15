package ProgrammigBatch;

public class program12 {
    public static void main(String[] args) {

            int star = 1;
            int line = 7;
            int ch=3;

            for (int a = 0; a < line; a++) {
                int ch1 = ch;
                for (int b = 0; b < star; b++) {
                    System.out.print(ch1++ + "\t");
                }
                System.out.println();
                if (a <= 2) {
                   ch--;
                   star++;
                } else {
                    ch++;
                    star--;
                }

            }
    }
}
