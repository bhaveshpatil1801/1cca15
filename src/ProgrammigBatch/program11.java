package ProgrammigBatch;

public class program11 {
    public static void main(String[] args) {
        int star = 1;
        int line = 7;

        for (int a = 0; a < line; a++) {
            int ch = 3;
            for (int b = 0; b < star; b++) {
                System.out.print(ch-- + "\t");
            }
            System.out.println();
            if (a <= 2) {
                star++;

            } else {
                star--;
            }

        }
    }
}
