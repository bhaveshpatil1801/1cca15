package ProgrammigBatch;

public class program14 {
    public static void main(String[] args) {
        int line=5;
        int star=5;
        int ch=1;

        for(int a=0;a<line;a++){
            int ch1=ch;
            for(int b=0;b<star;b++){
                System.out.print(ch1++);

                if(ch1>5){
                    ch1=5;
                }
            }
            System.out.println();
            ch++;
        }
    }
}
