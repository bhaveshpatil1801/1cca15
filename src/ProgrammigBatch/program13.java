package ProgrammigBatch;

public class program13 {
    public static void main(String[] args) {
        int line = 4;
        int space = 3;
        int star = 1;

        for (int a = 0; a < line; a++) {
            int ch = 1;
            for (int b = 0; b < space; b++) {
                System.out.println("");

                for (int c = 0; c < star; c++) {
                    if (a > c) {
                        System.out.print(ch--);
                    } else {
                        System.out.print(ch++);
                    }
                }
                System.out.println();
                ch++;
                star += 2;
                space--;
            }
        }

    }
}
