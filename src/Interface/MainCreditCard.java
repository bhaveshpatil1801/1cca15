package Interface;

public class MainCreditCard {
    public static void main(String[] args) {
        CreditCard credit;
        credit=new Visa();
        credit.getType();
        credit.withDraw(2500);
        System.out.println("=======================");
        credit=new Rupay();
        credit.getType();
        credit.withDraw(35000);
    }
}
