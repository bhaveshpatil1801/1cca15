package Looping;

public class breakLoopDemo1 {
    public static void main(String[] args) {
        for(int a=1;a<=10;a++){
            if(a==5){
                break;
            }
            System.out.println(a);
        }
        for(int a=1;a<=10;a++){
            if(a==5){
                continue;
            }
            System.out.println(a);
        }
    }
}
