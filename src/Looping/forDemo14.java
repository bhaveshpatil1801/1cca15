package Looping;

public class forDemo14 {
    public static void main(String[] args) {
        int line=5;
        int star=5;

        for(int a=0;a<line;a++){
            for(int b=0;b<star;b++){
                if(!(a==2||b==2)||a==b){
                    System.out.print(" * ");
                }
                else{
                    System.out.print("   ");
                }
            }
            System.out.println();
        }
    }
}
