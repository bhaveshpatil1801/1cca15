package Looping;
import java.util.Scanner;
public class forDemo2 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Start point");
        double start=sc1.nextDouble();
        System.out.println("End Point");
        double end=sc1.nextDouble();

        double sum=0.0;

        for(double a=start;a<=end;a++){
            if(a%2==0){
                sum=sum+a;
            }

        }
        System.out.println("Sum");
    }

}
