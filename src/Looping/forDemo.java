package Looping;

import java.util.Scanner;

public class forDemo {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter start");
        int start=sc1.nextInt();
        System.out.println("Enter end");
        int end=sc1.nextInt();

        for(int a=start;a<=end;a++){
            if(a%2==0){
                System.out.println(a*a);
            }
        }
    }
}
