package Looping;

public class forDemo4 {
    public static void main(String[] args) {
        int lines=5;
        int star=1;
        //outer for loop

        for(int row=1;row<=lines;row++)
        {
            for(int col=1;col<=star;col++)
            {
                System.out.print("*"+"\t");
            }

            System.out.println();
            star++;
        }


    }

}
