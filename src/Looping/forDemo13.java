package Looping;

public class forDemo13 {
    public static void main(String[] args) {

        int line =5;
        int star=5;

        for(int a=0;a<line;a++){
            for(int b=0;b<star;b++){

                if(a==0||b==0||a==b||a+b==4||a==4||b==4){

                    System.out.print(" * ");

                }
                else{
                    System.out.print("   ");
                }
            }
            System.out.println();
        }
    }
}
