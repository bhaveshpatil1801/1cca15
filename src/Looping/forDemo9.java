package Looping;

public class forDemo9 {
    public static void main(String[] args) {
        int line=5;
        int star=5;
                for(int a=0;a<line;a++){
                    for(int b=0;b<star;b++){
                        if(b==0||a==4||a+b==4){
                            System.out.print(" * ");
                        }
                        else{
                            System.out.print("   ");
                        }
                    }
                    System.out.println();
                }
    }
}
